# NFR-Guidelines


## Non-Functional Requirements Guidelines

## Description
This repository contains a document that provides guidelines to Business Analysts regarding how to capture Non-Functional Requirements during the requirements gathering phase. 

The guideline document provides the following: 
•	Provide a list and definition of commonly used non-functional requirements (NFR)
•	Questions that could be considered while eliciting the requirements for these NFR.
•	Sample requirements that can be stated as “specifications” that can be leveraged while eliciting requirements
•	Guidelines on how to validate and use the non functional requirements in a project  

The list provided in this document can be revised based on specific project needs and additional NFR can be added to the list as needed. The NFR listed in this document, must align with the Requirements Management Plan created for the project in which the Business Analyst is engaged.  

## Objective
Non-Functional Requirements are gathered from the business and technical users and these complement the functional requirements for the solution. Often these requirements are overlooked until the later stages of the project. This may lead to missed requirements which can result in a negative experience for the end user. Therefore, it is very important that the requirements team pays special attention in the elicitation of the non-functional requirements. 

## Intended Audience
This document is intended for new and existing Business Analysts and Business Architects, who are being on-boarded, in order to provide an understanding of non-functional requirements as well as provide a list of non-functional requirements which can be used as a point of reference when eliciting requirements during the requirements and architecture stage of a project. 
